import { Component } from "@angular/core";
import { FlexboxLayout } from "tns-core-modules/ui/layouts/flexbox-layout";
import * as dialogs from "ui/dialogs";

@Component({
  selector: "my-app",
  templateUrl: `./app.component.html`,
  styleUrls: ["app.component.css"]
})

export class AppComponent {
  count = 0;

  increment() {
    this.count++;
  }

  clear() {
    this.count = 0;
  }

  prompt() {
    dialogs.confirm({
        title: "Clear Counter",
        message: "Are you sure you want to clear the counter?",
        okButtonText: "Clear",
        cancelButtonText: "Cancel"
    }).then(result => {
        // result argument is boolean
        if (result == true)
        {
          this.count = 0;
        }
    });
  }

}
