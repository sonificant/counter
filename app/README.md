# Counter app made with NativeScript

## To get NativeScript up and running
https://docs.nativescript.org/start/quick-setup

## To test app of device or emulator
tns run android
OR
tns run ios

This repo uses NativeScript. Checkout NativeScript�s [Angular Getting Started Guide](https://docs.nativescript.org/angular/tutorial/ng-chapter-0).
